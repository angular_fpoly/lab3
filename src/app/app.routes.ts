import { Routes } from '@angular/router';
import {MainMenuComponent} from "./components/main-menu/main-menu.component";
import {Bai1Component} from "./components/bai1/bai1.component";
import {Bai2Component} from "./components/bai2/bai2.component";
import {Bai3Component} from "./components/bai3/bai3.component";
import {Bai4Component} from "./components/bai4/bai4.component";
import {Bai5Component} from "./components/bai5/bai5.component";

export const routes: Routes = [
  { path: '', component: MainMenuComponent},
  { path: 'bai1', component: Bai1Component},
  { path: 'bai2', component: Bai2Component},
  { path: 'bai3', component: Bai3Component},
  { path: 'bai4', component: Bai4Component},
  { path: 'bai5', component: Bai5Component},
];
