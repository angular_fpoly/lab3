import {Component} from '@angular/core';
import {MainLayoutComponent} from "../../layouts/main-layout/main-layout.component";
import {NgOptimizedImage} from "@angular/common";
import {productsData} from "../../data";

@Component({
  selector: 'app-bai1',
  standalone: true,
  imports: [
    MainLayoutComponent,
    NgOptimizedImage
  ],
  templateUrl: './bai1.component.html',
  styleUrl: './bai1.component.css'
})
export class Bai1Component {
  filter: string   = "";
  products: IProduct[] = productsData
}
