import { Component } from '@angular/core';
import {MainLayoutComponent} from "../../layouts/main-layout/main-layout.component";
import {productsData} from "../../data";
import {FormsModule} from "@angular/forms";

@Component({
  selector: 'app-bai2',
  standalone: true,
  imports: [
    MainLayoutComponent,
    FormsModule
  ],
  templateUrl: './bai2.component.html',
  styleUrl: './bai2.component.css'
})
export class Bai2Component {
  filter = '';
  products: IProduct[] = productsData;

  search() {
    if(this.filter.length <= 0) {
      this.setProductList = productsData;
    } else if(this.getProductList.length === 0) {
      this.setProductList = productsData;
    } else {
      this.setProductList = this.getProductList.filter((product) =>
        product.productName.toLowerCase().includes(this.filter.toLowerCase())
      );
    }
  }

  set setProductList(productList: IProduct[]) {
    this.products = productList;
  }

  get getProductList() {
    return this.products
  }
}
