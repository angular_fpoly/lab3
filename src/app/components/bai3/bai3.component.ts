import {Component} from '@angular/core';
import {MainLayoutComponent} from "../../layouts/main-layout/main-layout.component";
import {productsData} from "../../data";
import {StarComponent} from "../star/star.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

@Component({
  selector: 'app-bai3',
  standalone: true,
  imports: [
    MainLayoutComponent,
    StarComponent,
    ReactiveFormsModule,
    FormsModule
  ],
  templateUrl: './bai3.component.html',
  styleUrl: './bai3.component.css'
})
export class Bai3Component {
  filter = ''
  products: IProduct[] = productsData;
  search() {
    if(this.filter.length <= 0) {
      this.setProductList = productsData;
    } else if(this.getProductList.length === 0) {
      this.setProductList = productsData;
    } else {
      this.setProductList = this.getProductList.filter((product) =>
        product.productName.toLowerCase().includes(this.filter.toLowerCase())
      );
    }
  }

  set setProductList(productList: IProduct[]) {
    this.products = productList;
  }

  get getProductList() {
    return this.products
  }
}
