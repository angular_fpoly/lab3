import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Bai5Component } from './bai5.component';

describe('Bai4Component', () => {
  let component: Bai5Component;
  let fixture: ComponentFixture<Bai5Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [Bai5Component]
    })
    .compileComponents();

    fixture = TestBed.createComponent(Bai5Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
