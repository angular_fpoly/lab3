import {Component, Output, Input, EventEmitter, ViewChild} from '@angular/core';
import {MainLayoutComponent} from "../../layouts/main-layout/main-layout.component";
import {productsData} from "../../data";
import {Star2Component} from "../star2/star2.component";

@Component({
  selector: 'app-bai5',
  standalone: true,
  imports: [
    MainLayoutComponent,
    Star2Component
  ],
  templateUrl: './bai5.component.html',
  styleUrl: './bai5.component.css'
})
export class Bai5Component {
  notice!: string;
  products: IProduct[] = productsData;
  @ViewChild(Star2Component) starComponent!: Star2Component;
  @Input()
  filter: string = '';
  @Output()
  filterChange = new EventEmitter<string>();

  filterOnChange(event: Event): void {
    this.filter = (event.target as HTMLInputElement).value;
    this.filterChange.emit(this.filter);

    if (this.filter !== '') {
      const filterValue = this.filter.trim().toLowerCase();
      this.products = productsData.filter(product =>
        product.productName.toLowerCase().includes(filterValue)
      );
      if (this.products.length === 0) {
        this.products = productsData;
      }
    } else {
      this.products = productsData;
    }
  };

  handleRatingClicked(string : string) {
    console.log();
    // this.notice = this.starComponent.rating.toString();
    this.notice = string;
  }
}
