import {Component, EventEmitter, Input, NgZone, Output} from '@angular/core';

@Component({
  selector: 'app-star2',
  standalone: true,
  imports: [],
  templateUrl: './star2.component.html',
  styleUrl: './star2.component.css'
})
export class Star2Component {
  @Input() rating!: number | string;
  @Output() ratingClicked: EventEmitter<string> = new EventEmitter<string>();
  starWidth!: number;

  constructor(private zone: NgZone) {}

  onClick = () => {
    this.ratingClicked.emit(`Đánh giá của sản phẩm là ${this.rating} sao !`);
  }

  ngOnInit() {
    this.starWidth = (parseInt(this.rating as string) * 110) / 5;
  }

  ngOnChange() {
    this.starWidth = (parseInt(this.rating as string) * 110) / 5;
  }
}
